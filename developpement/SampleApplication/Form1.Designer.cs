﻿namespace SampleApplication
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btGetIDN = new System.Windows.Forms.Button();
            this.tbIDN = new System.Windows.Forms.TextBox();
            this.btSetProtocol15693 = new System.Windows.Forms.Button();
            this.tbProtocol = new System.Windows.Forms.TextBox();
            this.btSelectProtocol14443A = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btGetIDN
            // 
            this.btGetIDN.Location = new System.Drawing.Point(12, 12);
            this.btGetIDN.Name = "btGetIDN";
            this.btGetIDN.Size = new System.Drawing.Size(75, 23);
            this.btGetIDN.TabIndex = 0;
            this.btGetIDN.Text = "GetIDN";
            this.btGetIDN.UseVisualStyleBackColor = true;
            this.btGetIDN.Click += new System.EventHandler(this.btGetIDN_Click);
            // 
            // tbIDN
            // 
            this.tbIDN.Location = new System.Drawing.Point(93, 14);
            this.tbIDN.Name = "tbIDN";
            this.tbIDN.ReadOnly = true;
            this.tbIDN.Size = new System.Drawing.Size(124, 20);
            this.tbIDN.TabIndex = 1;
            // 
            // btSetProtocol15693
            // 
            this.btSetProtocol15693.Location = new System.Drawing.Point(12, 51);
            this.btSetProtocol15693.Name = "btSetProtocol15693";
            this.btSetProtocol15693.Size = new System.Drawing.Size(75, 44);
            this.btSetProtocol15693.TabIndex = 0;
            this.btSetProtocol15693.Text = "SetProtocol ISO15693";
            this.btSetProtocol15693.UseVisualStyleBackColor = true;
            this.btSetProtocol15693.Click += new System.EventHandler(this.btSetProtocol15693_Click);
            // 
            // tbProtocol
            // 
            this.tbProtocol.Location = new System.Drawing.Point(174, 63);
            this.tbProtocol.Name = "tbProtocol";
            this.tbProtocol.ReadOnly = true;
            this.tbProtocol.Size = new System.Drawing.Size(124, 20);
            this.tbProtocol.TabIndex = 1;
            // 
            // btSelectProtocol14443A
            // 
            this.btSelectProtocol14443A.Location = new System.Drawing.Point(93, 50);
            this.btSelectProtocol14443A.Name = "btSelectProtocol14443A";
            this.btSelectProtocol14443A.Size = new System.Drawing.Size(75, 45);
            this.btSelectProtocol14443A.TabIndex = 0;
            this.btSelectProtocol14443A.Text = "SetProtocol ISO14443A";
            this.btSelectProtocol14443A.UseVisualStyleBackColor = true;
            this.btSelectProtocol14443A.Click += new System.EventHandler(this.btSelectProtocol14443A_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbProtocol);
            this.Controls.Add(this.tbIDN);
            this.Controls.Add(this.btSelectProtocol14443A);
            this.Controls.Add(this.btSetProtocol15693);
            this.Controls.Add(this.btGetIDN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CR95HF Sample application";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGetIDN;
        private System.Windows.Forms.TextBox tbIDN;
        private System.Windows.Forms.Button btSetProtocol15693;
        private System.Windows.Forms.TextBox tbProtocol;
        private System.Windows.Forms.Button btSelectProtocol14443A;
    }
}

