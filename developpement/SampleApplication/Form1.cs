﻿using SDW.CR95HF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SampleApplication
{
    public partial class Form1 : Form
    {
        public static String SerialPort = "COM1";

        private SDW.CR95HF.CR95HFSerial m_cr95hf = new SDW.CR95HF.CR95HFSerial();

        public Form1()
        {
            InitializeComponent();
            if (!m_cr95hf.Start(SerialPort))
            {
                MessageBox.Show("Could not start interface to CR95HF");
            }
        }

        private void btGetIDN_Click(object sender, EventArgs e)
        {
            String idn = m_cr95hf.GetIDN();
            if (idn == null)
                MessageBox.Show("Error reading IDN");
            else
                tbIDN.Text = idn;
        }

        private void btSetProtocol15693_Click(object sender, EventArgs e)
        {
            ISO15693.Parameters parameters15693 = new ISO15693.Parameters();
            parameters15693.AppendCRC = true;
            parameters15693.Delay = ISO15693.Parameters.DelayEnum.WaitForSOF;
            parameters15693.Modulation = ISO15693.Parameters.ModulationEnum.Percent100;
            parameters15693.TransmissionDataRate = ISO15693.Parameters.TransmissionDataRateEnum.Kbps52;

            if (!m_cr95hf.ProtocolSelect(ISOProtocol.ProtocolCodes.ISO15693, parameters15693.ToByteArray()))
            {
                MessageBox.Show("Error setting protocol");
            }
            else
                tbProtocol.Text = "ISO15693";
        }

        private void btSelectProtocol14443A_Click(object sender, EventArgs e)
        {
            ISO14443A.Parameters parameters14443 = new ISO14443A.Parameters();
            parameters14443.ReceptionDataRate = ISO14443A.Parameters.EmissionReceptionDataRateEnum.Kbps106;
            parameters14443.TransmissionDataRate = ISO14443A.Parameters.EmissionReceptionDataRateEnum.Kbps106;

            if (!m_cr95hf.ProtocolSelect(ISOProtocol.ProtocolCodes.ISO14443A, parameters14443.ToByteArray()))
            {
                MessageBox.Show("Error setting protocol");
            }
            else
                tbProtocol.Text = "ISO14443A";
        }
    }
}

