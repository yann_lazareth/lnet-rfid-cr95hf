﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Serial interface protocol to RFID CR95HF chip in UART mode
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SDW.CR95HF
{
    public class CR95HFSerial
    {

        #region Protocol constants

        // Requests short information about the CR95HF and its revision.
        public const byte IDN_COMMAND = 0x01;
        // Selects the RF communication protocol and specifies certain protocol-related parameters.
        public const byte PROTOCOLSELECT_COMMAND = 0x02;
        // Sends data using the previously selected protocol and receives the tag response.
        public const byte SENDRECV_COMMAND = 0x04;
        // Switches the CR95HF into a low consumption Wait for Event (WFE) mode(Power-up, Hibernate, Sleep or Tag Detection), 
        // specifies the authorized wake-up sources and waits for an event to exit to Ready state.
        public const byte IDLE_COMMAND = 0x07;
        // Reads Wake-up event register or the Analog Register Configuration(ARC_B) register.
        public const byte READREG_COMMAND = 0x08;
        // Writes Analog Register Configuration (ARC_B)) register or writes index of ARC_B register address.
        // Writes the Timer Window(TimerW) value dedicated to ISO/IEC 14443 Type A tags.
        // Writes the AutoDetect Filter enable register dedicated to ISO/IEC 18092 tags
        public const byte WRITEREG_COMMAND = 0x09;
        // Sets the UART baud rate.
        public const byte BAUDRATE_COMMAND = 0x0A;
        // CR95HF returns an ECHO response (0x55).
        public const byte ECHO_COMMAND = 0x55;

       

        /// <summary>
        /// List of protocol error codes
        /// </summary>
        public enum ErrorCodes
        {
            NoError = 0,                // no error
            EEmdSOFerror23 = 0X63,      // SOF error in high part (duration 2 to 3 etu) in ISO/IEC 14443B
            EEmdSOFerror10 = 0x65,      //SOF error in low part(duration 10 to 11 etu) in ISO/IEC 14443B
            EEmdEgt = 0x66,             //error Extennded Guard Time error in ISO/IEC 14443B
            ETr1TooBigTooLong = 0x67,   // TR1 send by the card, reception stopped in ISO/IEC 14443BT
            ETr1TooSmall = 0x68,        // TR1 send by the card in ISO/IEC 14443B
            EinternalError = 0x71,      //Wrong frame format decodes
            EFrameRecvOK = 0x80,        //Frame correctly received(additionally see CRC/Parity
            EUserStop = 0x85,           //Stopped by user(used only in Card mode)
            ECommError = 0x86,          //Hardware communication error
            EFrameWaitTOut = 0x87,      //Frame wait time out (no valid reception)
            EInvalidSof = 0x88,         //Invalid SOF
            EBufOverflow = 0x89,        //Too many bytes received and data still arriving
            EFramingError = 0x8A,       //if start bit = 1 or stop bit = 0
            EEgtError = 0x8B,           //EGT time out
            EInvalidLen = 0x8C,         //Valid for ISO/IEC 18092, if Length<3
            ECrcError = 0x8D,           //CRC error, Valid only for ISO/IEC 18092
            ERecvLost = 0x8E,           //When reception is lost without EOF received (or subcarrier was
            ENoField = 0x8F,            //When Listen command detects the absence of external field
            EUnintByte = 0x90           //Residual bits in last byte. Useful for ACK/NAK reception of
        }

        #endregion

        #region Fields / Members

        // serial port object
        private SerialPort m_serial = null;
        // current selected protocol
        private ISOProtocol m_currentProtocol = null;

        #endregion

        #region Construction / Initialization

        /// <summary>
        /// Main constructor
        /// </summary>
        public CR95HFSerial()
        {
            m_serial = new SerialPort();
            m_serial.BaudRate = 57600;
            m_serial.DataBits = 8;
            m_serial.StopBits = StopBits.One;
            m_serial.Handshake = Handshake.None;
            m_serial.ReadTimeout = Globals.SERIAL_PORT_TIMEOUT;
        }

        /// <summary>
        /// Starts the comunication with the device
        /// </summary>
        /// <returns></returns>
        public bool Start(String serialPort)
        {
            try
            {
                if (m_serial.IsOpen)
                {
                    Tracer.Instance.LogError("Start(): Port already opened");
                    return false;
                }

                m_serial.PortName = serialPort;
                m_serial.Open();
                return m_serial.IsOpen;
            }
            catch (Exception e)
            {
                Tracer.Instance.LogError("Start(): " + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Stops the comunication with the device
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            try
            {
                if (!m_serial.IsOpen)
                {
                    Tracer.Instance.LogError("Stop(): Port not opened");
                    return false;
                }

                m_serial.Close();
                return true;
            }
            catch (Exception e)
            {
                Tracer.Instance.LogError("Stop(): " + e.ToString());
                return false;
            }
        }

        #endregion

        #region Tools

        /// <summary>
        /// Create a command frame
        /// The frame from the Host to the CR95HF has the following format: <CMD><Len><Data>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private byte[] CreateFrame(byte command, byte[] data)
        {
            if (data.Length > 255)
                return null;

            byte[] res = new byte[data.Length + 2];

            res[0] = command;
            res[1] = (byte)data.Length;
            Array.Copy(data, 0, res, 2, data.Length);

            return res;
        }

        /// <summary>
        /// Send frame to the serial port
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        private bool SendFrame(byte[] frame)
        {
            try
            {
                if (frame == null || frame.Length == 0)
                {
                    Tracer.Instance.LogError("SendFrame(): Frame is null or too short");
                    return false;
                }

                if (!m_serial.IsOpen)
                {
                    Tracer.Instance.LogError("SendFrame(): Port not opened");
                    return false;
                }

                m_serial.Write(frame, 0, frame.Length);

                return true;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("SendFrame(): " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Read next frame from serial up to end of transmission
        /// </summary>
        /// <returns>Frame read or null if timeout</returns>
        private byte[] ReadFrame()
        {
            int totalRead = 0;
            byte[] tmp = new byte[1024];

            try
            {
                int numberOfBytes = m_serial.Read(tmp, totalRead, 1024);

                while (numberOfBytes > 0)
                {
                    try
                    {
                        totalRead += numberOfBytes;
                        numberOfBytes = 0;
                        numberOfBytes = m_serial.Read(tmp, totalRead, 1024 - totalRead);
                    }
                    catch (System.TimeoutException ex)
                    {
                        // timeout no more data
                    }
                }

                if (totalRead > 0)
                {
                    byte[] frame = new byte[totalRead];
                    Array.Copy(tmp, 0, frame, 0, totalRead);
                    return frame;
                }

                return null;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("ReadFrame(): " + ex.ToString());
                return null;
            }            
        }

        #endregion

        #region Protocol methods

        /// <summary>
        /// Get the IDN of the device
        /// answer example : 0x010F4E4643204653324A41535434002ACE
        /// </summary>
        /// <returns></returns>
        public String GetIDN()
        {
            try
            {
                Tracer.Instance.LogInfo("GetIDN(): started");

                // create request frame
                byte []frame = CreateFrame(IDN_COMMAND, new byte[0]);
                SendFrame(frame);

                // wait the response to be ready
                Thread.Sleep(Globals.DELAY_WAIT_RESPONSE);

                // read response
                byte []response = ReadFrame();
                if (response != null)
                {
                    if (response.Length == 17)
                    {
                        String res = "";
                        for (int i = 0; i < 13; i++)
                        {
                            res += (char)response[i + 2];
                        }

                        Tracer.Instance.LogInfo("GetIDN(): succeed " + res);
                        return res;
                    }
                    else
                    {
                        Tracer.Instance.LogError("GetIDN(): Bad frame length");                        
                    }
                }

                // bad data
                return null;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("GetIDN(): " + ex.ToString());
                return null;
            }
        }        

        /// <summary>
        /// Select the protocol to be used
        /// answer example : 0x0000
        /// </summary>
        /// <returns></returns>
        public bool ProtocolSelect(ISOProtocol.ProtocolCodes protocol, byte[] parameters)
        {
            try
            {
                Tracer.Instance.LogInfo("ProtocolSelect(): started");

                // create request frame
                byte[] frame = CreateFrame(PROTOCOLSELECT_COMMAND, parameters);
                SendFrame(frame);

                // wait the response to be ready
                Thread.Sleep(Globals.DELAY_WAIT_RESPONSE);

                // read response
                byte[] response = ReadFrame();
                if (response != null)
                {
                    if (response.Length == 2)
                    {
                        if (response[0] == (byte)ErrorCodes.NoError)
                        {
                            Tracer.Instance.LogInfo("ProtocolSelect(): succeed ");
                            return true;
                        }
                        else
                        {
                            Tracer.Instance.LogError("ProtocolSelect(): Error code " + (ErrorCodes)response[0]);

                            switch (protocol)
                            {
                                case ISOProtocol.ProtocolCodes.ISO15693: m_currentProtocol = new ISO15693(this); break;
                                case ISOProtocol.ProtocolCodes.ISO14443A: m_currentProtocol = new ISO14443A(this); break;
                                default: m_currentProtocol = null; break;       // not supported yet
                            }
                            return true;
                        }
                    }
                    else
                    {
                        Tracer.Instance.LogError("ProtocolSelect(): Bad frame length");
                    }
                }

                // bad data
                return false;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("ProtocolSelect(): " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// This command sends data to a contactless tag and receives its reply.
        /// Before sending this command, the Host must first send the PROTOCOLSELECT command to
        /// select an RF communication protocol.
        /// If the tag response was received and decoded correctly, the<Data> field can contain
        /// additional information which is protocol-specific.
        /// </summary>
        /// <returns>Tag answer frame</returns>
        public byte[] SendReceive(byte []parameters)
        {
            try
            {
                Tracer.Instance.LogInfo("SendReceive(): started");

                // create request frame
                byte[] frame = CreateFrame(SENDRECV_COMMAND, parameters);
                SendFrame(frame);

                // wait the response to be ready
                Thread.Sleep(Globals.DELAY_WAIT_RESPONSE);

                // read response
                return ReadFrame();                
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("SendReceive(): " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Single inventory command on the current selected protocol
        /// </summary>
        /// <returns>0 if none, 8 bytes UID if found</returns>
        public ulong GetUID()
        {
            try
            {
                if (m_currentProtocol == null)
                    return 0;
                
                ulong uid = m_currentProtocol.GetTagUID();                

                return uid;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("GetUID(): " + ex.ToString());
                return 0;
            }
        }

        #endregion

    }
}
