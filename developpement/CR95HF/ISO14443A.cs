﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Implementation of the ISO 14443A protocol
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDW.CR95HF
{
    public class ISO14443A : ISOProtocol
    {
        #region Fields/Members

        #endregion

        #region Parameters class 

        /// <summary>
        /// ISO 14443 A protocol parameters
        /// </summary>
        public class Parameters
        {
            public enum EmissionReceptionDataRateEnum
            {
                Kbps106 = 0,
                Kbps212 = 1,
                Kbps424 = 2
            }

            public EmissionReceptionDataRateEnum TransmissionDataRate = EmissionReceptionDataRateEnum.Kbps106;
            public EmissionReceptionDataRateEnum ReceptionDataRate = EmissionReceptionDataRateEnum.Kbps106;
            /*public byte PP = 0;
            public byte MM = 0;
            public byte DD = 0;*/

            /// <summary>
            /// Transform parameters to byte array. To be used with ProtocolSelect() method
            /// Optionnal parameters are not implemented
            /// </summary>
            /// <returns>Byte array binding</returns>
            public byte[] ToByteArray()
            {
                byte[] res = new byte[2];

                byte trans = 0;
                switch (TransmissionDataRate)
                {
                    case EmissionReceptionDataRateEnum.Kbps106: trans = 0; break;
                    case EmissionReceptionDataRateEnum.Kbps212: trans = 0x40; break;
                    case EmissionReceptionDataRateEnum.Kbps424: trans = 0x80; break;
                }
                byte recep = 0;
                switch (ReceptionDataRate)
                {
                    case EmissionReceptionDataRateEnum.Kbps106: recep = 0; break;
                    case EmissionReceptionDataRateEnum.Kbps212: recep = 0x10; break;
                    case EmissionReceptionDataRateEnum.Kbps424: recep = 0x20; break;
                }

                res[0] = (byte)ProtocolCodes.ISO14443A;
                res[1] = (byte)(recep + trans);

                return res;
            }
        }

        #endregion

        #region Construction 

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="cr95">reference to the cr95hfserial owner object</param>
        public ISO14443A(CR95HFSerial cr95) : base(cr95)
        {

        }

        #endregion

        #region Internal protocol management

        #endregion

        #region Abstraction override

        /// <summary>
        /// Retreive a single UID of tag by starting inventory with single slot
        /// </summary>
        /// <returns>0 if no tag, else 8 bytes uuid</returns>
        public override ulong GetTagUID()
        {
            try
            {
                return 0;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("ISO14443A.GetTagUID():" + ex.ToString());
                return 0;
            }
        }

        #endregion

    }
}
