﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Globals 
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDW.CR95HF
{
    public class Globals
    {
        // debug mode
        public static bool DEBUG_MODE = true;
        // delay before reading response
        public static int DELAY_WAIT_RESPONSE = 1000;
        // timeout on serial port
        public static int SERIAL_PORT_TIMEOUT = 1000;
    }
}
