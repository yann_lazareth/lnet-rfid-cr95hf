﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Tracer allowing log frames to files and sending log events
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SDW.CR95HF
{
    public delegate void TracerEventHandler(String trace);

    public class Tracer
    {
        private static Tracer m_instance = new Tracer(Globals.DEBUG_MODE);
        private bool m_debugMode = false;

        public event TracerEventHandler EventNewTrace;
        protected void OnTrace(String trace)
        {
            if (EventNewTrace != null)
                EventNewTrace(trace);
        }

        private Tracer(bool debugMode)
        {
            m_debugMode = debugMode;
        }

        public static Tracer Instance
        {
            get { return m_instance; }
        }

        private void WriteInFile(String line)
        {
            lock (this)
            {
                //TextWriter file = File.AppendText("BlueBoxLF.txt");
                String baseLogFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SDW Tracer\\";
                String fileName = baseLogFolder + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + "CR95HF.txt";

                TextWriter file = File.AppendText(fileName);

                file.WriteLine(line);
                file.Close();
            }
        }

        public void Stop()
        {

        }

        public void LogError(String ch)
        {
            if (m_debugMode)
            {
                WriteInFile(DateTime.Now.ToString() + "\tERROR\t" + ch);
                Console.WriteLine(ch);
            }
            OnTrace(DateTime.Now.ToString() + "   Error : " + ch);
        }

        public void LogInfo(String ch)
        {
            if (m_debugMode)
            {
                WriteInFile(DateTime.Now.ToString() + "\tINFO\t" + ch);
                Console.WriteLine(ch);
            }
            OnTrace(DateTime.Now.ToString() + "   Info : " + ch);
        }

        public void LogDebug(String ch)
        {
            if (m_debugMode)
            {
                WriteInFile(DateTime.Now.ToString() + "\tDEBUG\t" + ch);
                Console.WriteLine(ch);
            }
            OnTrace(DateTime.Now.ToString() + "   Debug : " + ch);
        }
    }
}
