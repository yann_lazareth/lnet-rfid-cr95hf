﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Abstract class to defined the differents ISO protocols
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDW.CR95HF
{
    public abstract class ISOProtocol
    {
        #region Fields/Members

        /// <summary>
        /// List of protocol codes for protocol select command
        /// </summary>
        public enum ProtocolCodes
        {
            FieldOff = 0,
            ISO15693 = 1,
            ISO14443A = 2,
            ISO14443B = 3,
            ISO18092NFCType3 = 4,
            Undefined = 0xFF
        }

        // protocol type
        protected ProtocolCodes m_protocoleType = ProtocolCodes.Undefined;
        // pointer to cr95hf parent object
        protected CR95HFSerial m_cr95hf = null;

        #endregion

        #region Construction

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="cr95hf"></param>
        protected ISOProtocol(CR95HFSerial cr95hf)
        {
            m_cr95hf = cr95hf;
        }

        #endregion

        #region Abstraction

        /// <summary>
        /// Gets the single slot inventory frame
        /// </summary>
        /// <returns>0 if no tag, else 8 bytes uuid</returns>
        public abstract ulong GetTagUID();

        #endregion

    }
}
