﻿//-------------------------------------------------------------------------//
//
//  SDATAWAY
//      Implementation of the ISO 15693 protocol
//
//  CREATION : YLA 11/11/2021
//
//  MODIFICATIONS :
//     
//     
//-------------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDW.CR95HF
{
    public class ISO15693 : ISOProtocol
    {
        #region Fields/Members

        // command code 
        private byte ISO15693_CMDCODE_INVENTORY = 0x01;
        private byte ISO15693_CMDCODE_STAYQUIET = 0x02;
        private byte ISO15693_CMDCODE_READSINGLEBLOCK = 0x20;
        private byte ISO15693_CMDCODE_WRITESINGLEBLOCK = 0x21;
        private byte ISO15693_CMDCODE_LOCKBLOCK = 0x22;
        private byte ISO15693_CMDCODE_READMULBLOCKS = 0x23;
        private byte ISO15693_CMDCODE_WRITEMULBLOCKS = 0x24;
        private byte ISO15693_CMDCODE_SELECT = 0x25;
        private byte ISO15693_CMDCODE_RESETTOREADY = 0x26;
        private byte ISO15693_CMDCODE_WRITEAFI = 0x27;
        private byte ISO15693_CMDCODE_LOCKAFI = 0x28;
        private byte ISO15693_CMDCODE_WRITEDSFID = 0x29;
        private byte ISO15693_CMDCODE_LOCKDSFID = 0x2A;
        private byte ISO15693_CMDCODE_GETSYSINFO = 0x2B;
        private byte ISO15693_CMDCODE_GETSECURITYINFO = 0x2C;

        // mask of request flag
        private byte ISO15693_MASK_SUBCARRIERFLAG = 0x01;
        private byte ISO15693_MASK_DATARATEFLAG = 0x02;
        private byte ISO15693_MASK_INVENTORYFLAG = 0x04;
        private byte ISO15693_MASK_PROTEXTFLAG = 0x08;
        private byte ISO15693_MASK_SELECTORAFIFLAG = 0x10;
        private byte ISO15693_MASK_ADDRORNBSLOTSFLAG = 0x20;
        private byte ISO15693_MASK_OPTIONFLAG = 0x40;
        private byte ISO15693_MASK_RFUFLAG = 0x80;

        // request flags 
        private byte ISO15693_REQFLAG_SINGLESUBCARRIER = 0;
        private byte ISO15693_REQFLAG_TWOSUBCARRIER = 1;
        private byte ISO15693_REQFLAG_LOWDATARATE = 0;
        private byte ISO15693_REQFLAG_HIGHDATARATE = 1;
        private byte ISO15693_REQFLAG_INVENTORYFLAGNOTSET = 0;
        private byte ISO15693_REQFLAG_INVENTORYFLAGSET = 1;
        private byte ISO15693_REQFLAG_NOPROTOCOLEXTENSION = 0;
        private byte ISO15693_REQFLAG_PROTOCOLEXTENSION = 1;

        // request flag 5 to 8 definition when inventory flag is not set
        private byte ISO15693_REQFLAG_NOTSELECTED = 0;
        private byte ISO15693_REQFLAG_SELECTED = 1;
        private byte ISO15693_REQFLAG_NOTADDRESSES = 0;
        private byte ISO15693_REQFLAG_ADDRESSED = 1;
        private byte ISO15693_REQFLAG_OPTIONFLAGNOTSET = 0;
        private byte ISO15693_REQFLAG_OPTIONFLAGSET = 1;
        private byte ISO15693_REQFLAG_RFUNOTSET = 0;
        private byte ISO15693_REQFLAG_RFUSET = 1;

        // request flag 5 to 8 definition when inventory flag is set
        private byte ISO15693_REQFLAG_NOTAFI = 0;
        private byte ISO15693_REQFLAG_AFI = 1;
        private byte ISO15693_REQFLAG_16SLOTS = 0;
        private byte ISO15693_REQFLAG_1SLOT = 1;

        private byte ISO15693_NBBITS_MASKPARAMETER = 64;

        #endregion

        #region Parameters class 

        /// <summary>
        /// ISO 15693 protocol parameters
        /// </summary>
        public class Parameters
        {
            public enum SubCarrierEnum
            {
                Single,
                Dual
            }

            public enum ModulationEnum
            {
                Percent10,
                Percent100
            }

            public enum DelayEnum
            {
                Respect312us,
                WaitForSOF
            }

            public enum TransmissionDataRateEnum
            {
                Kbps26,
                Kbps52,
                Kbps6
            }

            public bool AppendCRC = false;
            public SubCarrierEnum SubCarrier = SubCarrierEnum.Single;
            public ModulationEnum Modulation = ModulationEnum.Percent100;
            public DelayEnum Delay = DelayEnum.Respect312us;
            public TransmissionDataRateEnum TransmissionDataRate = TransmissionDataRateEnum.Kbps52;

            /// <summary>
            /// Transform parameters to byte array. To be used with ProtocolSelect() method
            /// </summary>
            /// <returns>Byte array binding</returns>
            public byte[] ToByteArray()
            {
                byte[] res = new byte[2];

                byte append = (byte)(AppendCRC ? 1 : 0);
                byte subcar = (byte)(SubCarrier == SubCarrierEnum.Dual ? 2 : 0);
                byte mod = (byte)(Modulation == ModulationEnum.Percent10 ? 4 : 0);
                byte delay = (byte)(Delay == DelayEnum.WaitForSOF ? 8 : 0);
                byte speed = 0;
                switch (TransmissionDataRate)
                {
                    case TransmissionDataRateEnum.Kbps26: speed = 0; break;
                    case TransmissionDataRateEnum.Kbps52: speed = 16; break;
                    case TransmissionDataRateEnum.Kbps6: speed = 32; break;
                }

                res[0] = (byte)ProtocolCodes.ISO15693;
                res[1] = (byte)(append + subcar + mod + delay + speed);

                return res;
            }
        }

        #endregion

        #region Construction 

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="cr95">reference to the cr95hfserial owner object</param>
        public ISO15693(CR95HFSerial cr95) : base(cr95)
        {

        }

        #endregion

        #region Internal protocol management

        /// <summary>
        /// this function returns a Request flags on one byte, which is concatenation of inventory flags 
        /// SubCarrierFlag	: 0 for single subcarrier & 1 for double subcarrier   
        /// DataRateFlag	: 0 for 6 kbps & 0 for 26 kbps
        /// InventoryFlag	: 1 for inventory command & 0 for the others
        /// ProtExtFlag		: Not used by ISO 15693 (shall be reset (0 value))
        /// SelectOrAFIFlag	: the signification depends on InventoryFlag
        /// AddrOrNbSlotFlag: the signification depends on InventoryFlag
        /// OptionFlag		: depends on command 
        /// RFUFlag			: shall be reset (0 value)        
        /// </summary>
        private byte CreateRequestFlag(byte SubCarrierFlag, byte DataRateFlag, byte InventoryFlag, byte ProtExtFlag, byte SelectOrAFIFlag, byte AddrOrNbSlotFlag, byte OptionFlag, byte RFUFlag)
        {
            byte flagsByteBuf = (byte)(SubCarrierFlag & 0x01);
            flagsByteBuf += (byte)((DataRateFlag & 0x01) << 1);
            flagsByteBuf += (byte)((InventoryFlag & 0x01) << 2);
            flagsByteBuf += (byte)((ProtExtFlag & 0x01) << 3);
            flagsByteBuf += (byte)((SelectOrAFIFlag & 0x01) << 4);
            flagsByteBuf += (byte)((AddrOrNbSlotFlag & 0x01) << 5);
            flagsByteBuf += (byte)((OptionFlag & 0x01) << 6);
            flagsByteBuf += (byte)((RFUFlag & 0x01) << 7);

            return flagsByteBuf;
        }

        /// <summary>
        /// Gets the inventory frame
        /// </summary>
        /// <returns>inventory frame</returns>
        private byte[] GetInventoryRequest(byte flags, byte AFI, byte maskLength, byte []maskValue, bool appendCRC, ushort CRC16)
        {
            try
            {
                int ISO15693_MAXLENGTH_INVENTORY = 13;
                byte indexWrite = 0;
                byte nbMaskBytes = 0;
                byte nbSignificantBits = 0;
                byte firstByteMask;
                byte iMaskByte = 0;      
                
                byte[] tmpBuffer=new byte[ISO15693_MAXLENGTH_INVENTORY];                                

                // initialize the result code to 0xFF and length to 0  in case of error 
                if (maskLength > ISO15693_NBBITS_MASKPARAMETER)
                {
                    Tracer.Instance.LogError("ISO15693.GetInventoryRequest(): Mask length too long");
                    return null;
                }

                tmpBuffer[indexWrite++] = flags;
                tmpBuffer[indexWrite++] = ISO15693_CMDCODE_INVENTORY;

                if ((flags & ISO15693_MASK_SELECTORAFIFLAG) != 0)
                    tmpBuffer[indexWrite++] = AFI;

                tmpBuffer[indexWrite++] = maskLength;

                if (maskLength != 0)
                {
                    // compute the number of bytes of mask value(2 border exeptions)
                    if (maskLength == 64)
                        nbMaskBytes = 8;
                    else
                        nbMaskBytes = (byte)(maskLength / 8 + 1);

                    nbSignificantBits = (byte)(maskLength - (nbMaskBytes - 1) * 8);
                    if (nbSignificantBits != 0)
                        firstByteMask = (byte)((0x01 << nbSignificantBits) - 1);
                    else
                        firstByteMask = 0xFF;

                    // copy the mask value 
                    if (nbMaskBytes > 1)
                    {
                        for (iMaskByte = 0; iMaskByte < nbMaskBytes - 1; iMaskByte++)
                            tmpBuffer[indexWrite++] = maskValue[iMaskByte];
                    }

                    if (nbSignificantBits != 0)
                        tmpBuffer[indexWrite++] = (byte)(maskValue[iMaskByte] & firstByteMask);
                }

                if (!appendCRC)
                {
                    tmpBuffer[indexWrite++] = (byte)(CRC16 & 0xFF);
                    tmpBuffer[indexWrite++] = (byte)(CRC16 >> 8);
                }                
                
                // copy to return
                byte[] request = new byte[indexWrite];
                Array.Copy(tmpBuffer, 0, request, 0, indexWrite);

                return request;
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("ISO15693.GetInventoryRequest():" + ex.ToString());
                return null;
            }
        }

        #endregion

        #region Abstraction override


        /// <summary>
        /// Retreive a single UID of tag by starting inventory with single slot
        /// </summary>
        /// <returns>0 if no tag, else 8 bytes uuid</returns>
        public override ulong GetTagUID()
        {
            try
            {
                byte flag = CreateRequestFlag(ISO15693_REQFLAG_SINGLESUBCARRIER,
                                            ISO15693_REQFLAG_HIGHDATARATE,
                                            ISO15693_REQFLAG_INVENTORYFLAGSET,
                                            ISO15693_REQFLAG_NOPROTOCOLEXTENSION,
                                            ISO15693_REQFLAG_NOTAFI,
                                            ISO15693_REQFLAG_1SLOT,
                                            ISO15693_REQFLAG_OPTIONFLAGNOTSET,
                                            ISO15693_REQFLAG_RFUNOTSET);

                byte[] requestFrame = GetInventoryRequest(flag, 0x00, 0x00, new byte[0], true, 0x0);

                byte[] response = m_cr95hf.SendReceive(requestFrame);

                if (response == null)
                {
                    Tracer.Instance.LogError("ISO15693.GetTagUID(): No response");
                    return 0;
                }

                // check response
                if (response.Length >= 11)
                {
                    if (response[0] == 0x80)  // success
                    {
                        // uid start at index 4 and len is 8 bytes
                        ulong uid = 0;
                        for (int i = 0; i < 8; i++)
                        {
                            ulong b = ((ulong)response[4 + i]) << (8 - i + 1);
                            uid += b;
                        }
                        return uid;
                    }
                    else
                    {
                        Tracer.Instance.LogError("ISO15693.GetTagUID(): Error code " + response[0]);
                        return 0;
                    }
                }
                else
                {
                    Tracer.Instance.LogError("ISO15693.GetTagUID(): response frame is too short");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Tracer.Instance.LogError("ISO15693.GetTagUID():" + ex.ToString());
                return 0;
            }
        }


        #endregion

    }
}
